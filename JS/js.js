const themeBtn = document.querySelector(".theme");
const heading = document.querySelector(".heading");

document.documentElement.style.setProperty(
  "--primary-text-color",
  localStorage.getItem("primaryTextColor")
);

heading.style.backgroundColor = localStorage.getItem("navBarColor");

themeBtn.addEventListener("click", (e) => {
  if (localStorage.getItem("themeColor") === "white") {
    document.documentElement.style.setProperty("--primary-text-color", "black");
    heading.style.backgroundColor = "rgba(94, 94, 94, 0.4)";
    localStorage.setItem("themeColor", "black");
    localStorage.setItem("primaryTextColor", "#000000");
    localStorage.setItem("navBarColor", "rgba(94, 94, 94, 0.4)");
  } else {
    document.documentElement.style.setProperty(
      "--primary-text-color",
      "#ffffff"
    );
    heading.style.backgroundColor = "rgba(0, 0, 0, 0.4)";
    localStorage.setItem("themeColor", "white");
    localStorage.setItem("primaryTextColor", "#ffffff");
    localStorage.setItem("navBarColor", "rgba(0, 0, 0, 0.4)");
  }
});
